﻿using UnityEngine;
using System;

public class EquipmentManager : MonoBehaviour {

    public static EquipmentManager instance;
    public delegate void OnEquipmentChange(Equipment newItem, Equipment oldItem);
    public OnEquipmentChange onEquipmentChange;
    public SkinnedMeshRenderer targetMesh;
    public Equipment[] defaultItems;

    // the equipment equipped
    Equipment[] currentEquipment;
    SkinnedMeshRenderer[] currentMeshes;
    Inventory inventory;

    void Awake() {
        instance = this;
    }

    void Start() {
		inventory = Inventory.instance;
        int numSlots = Enum.GetNames(typeof(EquipmentSlot)).Length;
        // initialize the array with the total amount of the equipment slots and meshes
        currentEquipment = new Equipment[numSlots];
        currentMeshes = new SkinnedMeshRenderer[numSlots];

        EquipDefaultItems();
    }

	void Update() {
		if (Input.GetKeyDown(KeyCode.U)) {
			UnequipAll();
		}
	}

    // add an equip
    public void Equip(Equipment newItem) {
        // obtain the index in the enum with the equipment slots
        int slotIndex = (int)newItem.equipSlot;

		// re-add the old element in the inventory
		Equipment oldItem = Unequip(slotIndex);

        // if someone call it
        if (onEquipmentChange != null) {
            onEquipmentChange.Invoke(newItem, oldItem);
        }

        SetEquipmentBlendShapes(newItem, 100);

        currentEquipment[slotIndex] = newItem;
        // create a new mesh and assign the target mesh like parent
        SkinnedMeshRenderer newMesh = Instantiate(newItem.mesh);
        newMesh.transform.parent = targetMesh.transform;
        // set the mesh
        newMesh.bones = targetMesh.bones;
        newMesh.rootBone = targetMesh.rootBone;
        currentMeshes[slotIndex] = newMesh;
    }

    // just unequip the equipment
    public Equipment Unequip(int slotIndex) {
        if (currentEquipment[slotIndex] != null) {
            if (currentMeshes[slotIndex] != null) {
                Destroy(currentMeshes[slotIndex].gameObject);
            }
            Equipment oldItem = currentEquipment[slotIndex];
            inventory.Add(oldItem);
            currentEquipment[slotIndex] = null;

            // if someone call it
			if (onEquipmentChange != null) {
				onEquipmentChange.Invoke(null, oldItem);
			}
            return oldItem;
        }
        return null;
    }

    // unequip all the equipments
    public void UnequipAll() {
        for (int i = 0; i < currentEquipment.Length; i++) {
            Unequip(i);
        }
        //reequip the default items
        EquipDefaultItems();
    }

    public void SetEquipmentBlendShapes(Equipment item, int weight) {
        foreach (EquipmentMeshRegion blendShape in item.coveredMeshReagion) {
            // cast to integer to have the index
            targetMesh.SetBlendShapeWeight((int)blendShape, weight);
        }
    }

    // equip the default items
    public void EquipDefaultItems() {
        foreach (Equipment item in defaultItems) {
            Equip(item);
        }
    }
}
