﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour {

    public float attackSpeed = 1f;
    public float attackDelay = 0.6f;

    // notify with a delegate when player attacks
    public event Action OnAttack;

    // stats of the player
    CharacterStats myStats;
    float attackCooldown;

    void Start() {
        myStats = GetComponent<CharacterStats>();
    }

    void Update() {
        attackCooldown -= Time.deltaTime;
    }

    // attack the enemy with its stats target
    public void Attack(CharacterStats targetStats) {
        // enemy take damage
		if (attackCooldown <= 0f) {
            StartCoroutine(DoDamage(targetStats, attackDelay));
            if (OnAttack != null) {
                OnAttack();
            }
            // wait attack speed cooldown
            attackCooldown = 1f / attackSpeed;
		}
    }

    // do damage after a certain delay of animation
    IEnumerator DoDamage(CharacterStats stats, float delay) {
        yield return new WaitForSeconds(delay);
        stats.TakeDamage(stats.damage.GetValue());
    }
}
