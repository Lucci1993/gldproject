﻿using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour {

    private const float locomotionAnimationSmoothTime = 0.1f;

    private Animator animator;
    private NavMeshAgent agent;

    private void Start() {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }

    private void Update() {
        // velocity of the player in percentage
        float speedPercent = agent.velocity.magnitude / agent.speed;
        // change the animation based on the velocity
        animator.SetFloat("speedPercent", speedPercent, locomotionAnimationSmoothTime, Time.deltaTime);
    }
}
