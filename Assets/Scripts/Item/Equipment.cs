﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Equipment")]
public class Equipment : Item {

    public EquipmentSlot equipSlot;
    public SkinnedMeshRenderer mesh;
    public EquipmentMeshRegion[] coveredMeshReagion;
    public int armorModifier;
    public int damageModifier;

    public override void Use() {
        base.Use();
        EquipmentManager.instance.Equip(this);
        RemoveFromInventory();
    }
}

// where to apply the equipment
public enum EquipmentSlot {
    Head,
    Chest,
    Legs,
    Weapon,
    Shield,
    Feet
}

// where to apply the mesh deformation
public enum EquipmentMeshRegion {
	Legs,
	Arms,
	Torso
}