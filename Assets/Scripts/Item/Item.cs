﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject {

    // override the old definition
    new public string name = "New Item";
    public Sprite icon = null;
    public bool isDefaultItem = false;

    // for different item we want different things to happened
    public virtual void Use() {
        
    }

    // remove an element from the inventory
    public void RemoveFromInventory() {
        Inventory.instance.Remove(this);
    }
}
