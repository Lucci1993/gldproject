﻿using UnityEngine;

public class ItemPickup : Interactable {

    public Item item;

    public override void Interact() {
        base.Interact();
        PickUp();
    }

    // pick up the object
    private void PickUp() {
        // Add to Inventory if is not full
        if (Inventory.instance.Add(item)) {
            Destroy(gameObject);
        }
    }
}
