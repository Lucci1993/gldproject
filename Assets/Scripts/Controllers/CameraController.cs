﻿using UnityEngine;

public class CameraController : MonoBehaviour {

	public Transform target;
	public Vector3 offset;
	// height of the player
	public float pitch = 2f;
	// rotation speed
	public float angularSpeed = 100f;
	// change the zoom
	public float zoomSpeed = 4f;
	public float minZoom = 5f;
	public float maxZoom = 15f;

	// start zoom
	private float currentZoom = 10f;
	private float currentAngle = 0f;

	private void Update() {
		// zooming
		currentZoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
		// between range
		currentZoom = Mathf.Clamp(currentZoom, minZoom, maxZoom);
		// set the angle rotation of the camera
		currentAngle -= Input.GetAxis("Horizontal") * angularSpeed * Time.deltaTime;
	}

	// Update is called once per frame
	private void LateUpdate () {
		// move camera
		transform.position = target.position - offset * currentZoom;
		// rotate camera and take count of the target height
		transform.LookAt(target.position + Vector3.up * pitch);
		// rotate camera with certain velocity
		transform.RotateAround(target.position, Vector3.up, currentAngle);
	}
}
