using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public delegate void OnItemChanged();

    public List<Item> items = new List<Item>();
    public OnItemChanged onItemChangedCallback;

    // add singleton
    public static Inventory instance;
    public int space = 20;

    private void Awake() {
        // set the singleton
        if (instance != null) {
            return;
        }
        instance = this;
    }

    // add an item to the inventory
    public bool Add(Item item) {
        if (!item.isDefaultItem) {
            // just if is not full
            if (items.Count < space) {
				items.Add(item);
                // check if a method subscribe our callback
                if (onItemChangedCallback != null) {
                    onItemChangedCallback.Invoke();
                }
                return true;
            }
        }
        return false;
    }

    // remove an item from the inventory
    public void Remove(Item item) {
        items.Remove(item);
		// check if a method subscribe our callback
		if (onItemChangedCallback != null) {
			onItemChangedCallback.Invoke();
		}
    }
}
