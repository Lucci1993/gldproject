﻿using UnityEngine.UI;
using UnityEngine;

public class InventorySlot : MonoBehaviour {

    public Image icon;
    public Button removeButton;

    Item item;

    // add an item to the inventory adn set the parameter of the slot
    public void AddItem(Item newItem) {
        item = newItem;

        icon.sprite = item.icon;
        icon.enabled = true;
        removeButton.interactable = true;
    }

    // clear a slot
    public void ClearSlot() {
        item = null;

		icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
    }

    // remove item
    public void OnRemoveButton() {
        Inventory.instance.Remove(item);
    }

    // use the item if exist
    public void UseItem() {
        if (item != null) {
            item.Use();
        }
    }
}
